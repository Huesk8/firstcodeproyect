﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FirstCodeProyect.FronEnd.Startup))]
namespace FirstCodeProyect.FronEnd
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
