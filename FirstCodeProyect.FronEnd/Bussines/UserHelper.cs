﻿namespace FirstCodeProyect.FronEnd.Bussines
{
    using FirstCodeProyect.Model.Models;
    using System;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using FirstCodeProyect.FronEnd.Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    public class UserHelper : IDisposable
    {
        private static readonly ApplicationDbContext _userContext = new ApplicationDbContext();
        private static readonly DataContextLocal _db = new DataContextLocal();

        internal static void CheckRol(string RoleName)
        {
            var rolmanager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(_userContext));

            if (!rolmanager.RoleExists(RoleName))
            {
                var result = rolmanager.Create(new IdentityRole(RoleName));
                if (result.Succeeded)
                {
                    addRolDataBase(RoleName);
                }
            }
        }

        internal static void CheckGender()
        {
           var lst = new List<string> { "Hombre", "Mujer" };

            foreach (var item in lst)
            {
                var gender = _db.Genders.FirstOrDefault(x => x.Description.Equals(item));
                if (gender == null)
                {
                    var addGender = new Gender
                    {
                        Description = item
                    };
                    _db.Genders.Add(addGender);
                    _db.SaveChanges();
                }

            }

        }

        private static void addRolDataBase(string roleName)
        {
            var rol = _db.Roles.FirstOrDefault(x => x.Description.Equals(roleName));
            if (rol != null)
                return;

            var addrol = new Role
            {
                Description = roleName
            };

            _db.Roles.Add(addrol);
            _db.SaveChanges();

        }


        /// <summary>
        /// Libera los recursos de Base de Datos.
        /// </summary>
        public void Dispose()
        {
            _userContext.Dispose();
            _db.Dispose();
        }

    }
}