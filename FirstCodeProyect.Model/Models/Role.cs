﻿namespace FirstCodeProyect.Model.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
     public class Role
    {
        public Role()
        {
            RoleKey = Guid.NewGuid();
        }

        [Key]
        public int RoleId { get; set; }
        public Guid RoleKey { get; set; }
        public string Description { get; set; }
        public ICollection<User> Users { get; set; }
    }
}
