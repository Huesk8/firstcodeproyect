﻿namespace FirstCodeProyect.Model.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

     public class Gender
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Gender()
        {
            GenderKey = Guid.NewGuid();
        }

        [Key]
        public int GenderId { get; set; }
        public Guid GenderKey { get; set; }
        public string Description { get; set; }
        public ICollection<User> Users { get; set; }

    }
}
