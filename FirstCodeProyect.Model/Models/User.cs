﻿
namespace FirstCodeProyect.Model.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

     public class User
    {
        public User()
        {
            UserKey = Guid.NewGuid();
            IsActive = true;
            DateCreate = DateTime.Now;
        }

        [Key]
        public int UserId { get; set; }
        public Guid UserKey { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{Name} {LastName}";
        public string Email { get; set; }
        public int GenderId { get; set; }
        public int RoleId { get; set; }
        public bool IsActive { get; set; }
        public string UserName { get; set; } 
        public DateTime DateCreate { get; set; }
        public virtual Role Role { get; set; }
        public virtual Gender Gender { get; set; }

    }
}
