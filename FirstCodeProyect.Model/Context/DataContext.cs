﻿using System.Data.Entity.ModelConfiguration;

namespace FirstCodeProyect.Model.Context
{
    using FirstCodeProyect.Model.Models;
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;

    public class DataContext : DbContext
    {
        //constructor 
        public DataContext() : base("DefaultConnection")
        {
            this.Configuration.ProxyCreationEnabled = false;
            this.Configuration.LazyLoadingEnabled = false;
        }

        #region Metodos
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //deshabilitar la opcion de eliminacion en cascada
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            //Mapear tablas 
            modelBuilder.Configurations.Add(new UserMap());

        }
        #endregion

        #region Propiedades
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Gender> Genders { get; set; }
        #endregion

    }

    internal class UserMap : EntityTypeConfiguration<User>
    {
        //costructor
        public UserMap()
        {
            HasRequired(x => x.Role)
                .WithMany(x => x.Users)
                .HasForeignKey(x => x.RoleId);

            HasRequired(x => x.Gender)
                .WithMany(x => x.Users)
                .HasForeignKey(x => x.GenderId);

        }
    }
}
